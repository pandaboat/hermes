# Hermes

Hermes is a plugin for Bukkit / Spigot which will give players an increased running speed when one of the following conditions is meet:
* The player is moving on a grass path block and the block below is not soul sand
* The block below the block the player is moving on is packed ice

The speed effect is only applied if the player does not already have a speed effect on themself.

# Installation

The plugin requires the Kotlin standard library to be present on the classpath to function. You can either add it directtly to your Spigot start script or use the [Kotlin Plugin](https://gitlab.com/pandaboat/kotlin/).

Just copy the `hermes-$VERSION.jar` to your`./plugins` folder and reload the server

# Contributions

Any contributions are welcome - just open a [merge request](https://gitlab.com/pandaboat/hermes/-/merge_requests).

# License

The plugin is released under the MIT license. See [LICENSE](./LICENSE) for more details.