package com.pandaboat.hermes

import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.plugin.java.annotation.dependency.Dependency
import org.bukkit.plugin.java.annotation.plugin.Plugin
import org.bukkit.plugin.java.annotation.plugin.author.Author

@Plugin(name = "Hermes", version = "0.1.0")
@Author("Michael Dürgner")
class HermesPlugin: JavaPlugin() {

    override fun onEnable() {
        super.onEnable()
        server.pluginManager.registerEvents(HermesListener(this), this)
    }
}