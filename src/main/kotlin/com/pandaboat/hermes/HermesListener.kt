package com.pandaboat.hermes

import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityPotionEffectEvent
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.metadata.FixedMetadataValue
import org.bukkit.plugin.Plugin
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType

class HermesListener(private val plugin: Plugin): Listener {
    
    @EventHandler(priority = EventPriority.HIGH)
    fun onPlayerMove(event: PlayerMoveEvent) {
        val player = event.player
        if (event.from == event.to) {
            return
        }
        val hermesActive = player.hasMetadata("plugin::de.cfcraft.hermes", plugin)
        if((hermesActive || event.from.isHermesBlock()) && event.to?.isHermesBlock() == true) {
            if (player.hasPotionEffect(PotionEffectType.SPEED) && !hermesActive) {
                return
            }
            player.removePotionEffect(PotionEffectType.SPEED);
            player.addPotionEffect(PotionEffect(PotionEffectType.SPEED, 20, 2))
            player.setMetadata("plugin::de.cfcraft.hermes", FixedMetadataValue(plugin, true))
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    fun onPotionExpired(event: EntityPotionEffectEvent) {
        if (event.entity is Player && event.oldEffect?.type == PotionEffectType.SPEED && event.newEffect == null) {
            (event.entity as Player).removeMetadata("plugin::de.cfcraft.hermes", plugin)
        }
    }
}

private fun Player.hasMetadata(key: String, plugin: Plugin) = this.getMetadata(key).find { it.owningPlugin == plugin } != null

private fun Block.on(): Block = when(this.type) {
    Material.GRASS_PATH -> this
    else -> this.location.subtract(0.0,0.5,0.0).block
}

private fun Block.below(): Block = when(this.type) {
    Material.GRASS_PATH -> this.location.subtract(0.0,1.0,0.0).block
    else -> this.location.subtract(0.0, 1.5, 0.0).block
}

private fun Block.isType(vararg types: Material) = types.contains(this.type)

private fun Location.isHermesBlock() = (this.block.on().isType(Material.GRASS_PATH) && !this.block.below().isType(Material.SOUL_SAND)) || this.block.below().isType(Material.PACKED_ICE)
